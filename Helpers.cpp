#include "Helpers.h"
#include <math.h>

double korensum(double a, double b)
{
	return pow(a, 2) + 2 * a * b + pow(b, 2);
}